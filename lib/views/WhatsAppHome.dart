import 'package:flutter/material.dart';
import 'package:flutter_whatsapp/views/pages/calls_screen.dart';
import 'package:flutter_whatsapp/views/pages/camera_screen.dart';
import 'package:flutter_whatsapp/views/pages/chats_screen.dart';
import 'package:flutter_whatsapp/views/pages/status_screen.dart';

class WhatsAppHome extends StatefulWidget {
    _WhatsAppHomeState createState() => new _WhatsAppHomeState();
}

class _WhatsAppHomeState extends State<WhatsAppHome>
    with SingleTickerProviderStateMixin {

    TabController _tabController;

    @override
      void initState() {
        super.initState();
        _tabController = new TabController(length: 4, vsync: this, initialIndex: 1);
      }

    @override
      Widget build(BuildContext context) {
        return new Scaffold(
            appBar: new AppBar(
                title: new Text("WhatsApp", style: new TextStyle(fontSize: 19.0),),
                elevation: 0.7,
                bottom: new TabBar(
                    controller: _tabController,
                    indicatorColor: Colors.white,
                    tabs: <Widget>[
//                        new Container(
//                            child: new Tab(
//                                icon: new Icon(Icons.camera_alt),
//                            ),
//
//                        ),
                        new Tab(
                            icon: new Icon(Icons.camera_alt),
                        ),
                        new Tab(text: "CHATS",),
                        new Tab(text: "STATUS",),
                        new Tab(text: "CALLS",),
                    ],
                ),
                actions: <Widget>[
                    new Icon(Icons.search),
                    new Padding(padding: const EdgeInsets.symmetric(horizontal: 6.0),),
                    new Icon(Icons.more_vert),
                    new Padding(padding: const EdgeInsets.symmetric(horizontal: 5.0),),
                ],
            ),
            body: new TabBarView(
                controller: _tabController,
                children: <Widget>[
                    new CameraScreen(),
                    new ChatsScreen(),
                    new StatusScreen(),
                    new CallsScreen(),
                ],
            ),
            floatingActionButton: new FloatingActionButton(
                backgroundColor: Theme.of(context).accentColor,
                child: new Icon(Icons.message, color: Colors.white,),
                onPressed: () => print("open chats"),
            ),
        );
      }
}