class ChatsModal {
    final String name;
    final String message;
    final String time;
    final String avatarUrl;

    // Constructor
    ChatsModal({this.name, this.message, this.time, this.avatarUrl});
}
List<ChatsModal> dummyData = [
    new ChatsModal(
        name: "F34th3R Owl",
        message: "Nothing to say",
        time: "3:10 PM",
        avatarUrl: "https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&h=650&w=940 1x, https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940 2x",
    ),
    new ChatsModal(
        name: "F34th3R Owl",
        message: "Nothing to say",
        time: "3:05 PM",
        avatarUrl: "https://images.pexels.com/photos/119342/pexels-photo-119342.jpeg?auto=compress&cs=tinysrgb&h=650&w=940%201x,%20https://images.pexels.com/photos/119342/pexels-photo-119342.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940%202x",
    ),
    new ChatsModal(
        name: "F34th3R Owl",
        message: "Nothing to say",
        time: "3:05 PM",
        avatarUrl: "https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&h=650&w=940 1x, https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940 2x",
    ),
    new ChatsModal(
        name: "F34th3R Owl",
        message: "Nothing to say",
        time: "3:05 PM",
        avatarUrl: "https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&h=650&w=940 1x, https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940 2x",
    ),
    new ChatsModal(
        name: "F34th3R Owl",
        message: "Nothing to say",
        time: "3:05 PM",
        avatarUrl: "https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&h=650&w=940 1x, https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940 2x",
    ),
    new ChatsModal(
        name: "F34th3R Owl",
        message: "Nothing to say",
        time: "3:05 PM",
        avatarUrl: "https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&h=650&w=940 1x, https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940 2x",
    ),
    new ChatsModal(
        name: "F34th3R Owl",
        message: "Nothing to say",
        time: "3:05 PM",
        avatarUrl: "https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&h=650&w=940 1x, https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940 2x",
    ),
    new ChatsModal(
        name: "F34th3R Owl",
        message: "Nothing to say",
        time: "3:05 PM",
        avatarUrl: "https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&h=650&w=940 1x, https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940 2x",
    ),
    new ChatsModal(
        name: "F34th3R Owl",
        message: "Nothing to say",
        time: "3:05 PM",
        avatarUrl: "https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&h=650&w=940 1x, https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940 2x",
    ),
    new ChatsModal(
        name: "F34th3R Owl",
        message: "Nothing to say",
        time: "3:05 PM",
        avatarUrl: "https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&h=650&w=940 1x, https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940 2x",
    ),
    new ChatsModal(
        name: "F34th3R Owl",
        message: "Nothing to say",
        time: "3:05 PM",
        avatarUrl: "https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&h=650&w=940 1x, https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940 2x",
    ),
    new ChatsModal(
        name: "F34th3R Owl",
        message: "Nothing to say",
        time: "3:05 PM",
        avatarUrl: "https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&h=650&w=940 1x, https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940 2x",
    ),
    new ChatsModal(
        name: "F34th3R Owl",
        message: "Nothing to say",
        time: "3:05 PM",
        avatarUrl: "https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&h=650&w=940 1x, https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940 2x",
    ),
    new ChatsModal(
        name: "F34th3R Owl",
        message: "Nothing to say",
        time: "3:05 PM",
        avatarUrl: "https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&h=650&w=940 1x, https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940 2x",
    ),
    new ChatsModal(
        name: "F34th3R Owl",
        message: "Nothing to say",
        time: "3:05 PM",
        avatarUrl: "https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&h=650&w=940 1x, https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940 2x",
    ),
    new ChatsModal(
        name: "F34th3R Owl",
        message: "Nothing to say",
        time: "3:05 PM",
        avatarUrl: "https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&h=650&w=940 1x, https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940 2x",
    ),
    new ChatsModal(
        name: "F34th3R Owl",
        message: "Nothing to say",
        time: "3:05 PM",
        avatarUrl: "https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&h=650&w=940 1x, https://images.pexels.com/photos/91224/pexels-photo-91224.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940 2x",
    ),

];